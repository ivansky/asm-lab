        extern _printf
    
        SECTION .data                           ; Data section

fmts:   db "%s !", 10, 0
fmta:   db "%d %s !", 10, 0
fmtd:   db "%d", 10, 0
hello:  db "hello", 10, 0
counter: dd 0

        SECTION .text                           ; code section

        global main                             ; make label available to linker 
main:                                           ; standard entry point
        push    ebp
        mov     ebp, esp
        
        push    dword [ebp + 8]
        push    dword fmtd
        call    _printf
        add     esp, 2*4

        mov     ebx, 1

next_arg:

        mov     edx, dword [ebp + 12]
        add     edx, dword [counter]

        push    dword [edx]
        push    dword fmts
        call    _printf
        add     esp, 2*4

        mov     eax, dword [counter]
        add     eax, 4
        mov     [counter], eax

        inc     ebx

        cmp     ebx, dword [ebp + 8]
        jl      next_arg

exit:
        mov     eax, 0			        ; exit code, 0=normal
        mov     esp, ebp                        ; pop
        pop     ebp                             ; stack frame
        ret				        ; main returns to operating system
