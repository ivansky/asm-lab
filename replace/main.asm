DEFAULT REL

section .data
        msg     db "HELLO", 0
        format_s  db "STRING: %s", 10, 0
        format_d  db "DECIMAL: %d", 10, 0

section .text
        global main
        extern _printf

main:
        mov rdi, format_s
        mov rsi, msg

        mov rax, 0
        call _printf

        jmp exit

my_strlen:
        mov rcx, -1
        mov rsi, rdi ; backup rdi
        mov al, 0    ; look for \0
        repne scasb  ; actually do the search
        sub rdi, rsi ; save the string length
        dec rdi      ; don't count the \0 in the string length
        mov rax, rdi ; save the return value
        ret

exit: 
        mov rax, 0x2000001
        mov rdi, 0
        syscall